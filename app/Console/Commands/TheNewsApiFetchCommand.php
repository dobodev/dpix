<?php

namespace App\Console\Commands;

use App\Events\NewsApiFetchEvent;
use App\Models\ApiNews;
use App\Services\TheNewsApiService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TheNewsApiFetchCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dobodev:the-news-api-fetch {newsCounter?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch data from thenewsapi.com';

    /**
     * Execute the console command.
     */
    public function handle(TheNewsApiService $theNewsApiService)
    {
        // dd($this->arguments());
        $response = $theNewsApiService->getAPIData();

        $bar = $this->output->createProgressBar(count($response));

        $bar->start();
        if (is_array($response) && ! empty($response)) {
            foreach ($response as $bulk => $item) {
                if (ApiNews::where('uuid', $item['uuid'])->exists()) {
                    Log::info('Fetched record, but data exists: '.$item['uuid']);

                    return;
                }

                Log::info('New record saved to DB: '.$item['uuid']);
                $apiNews = ApiNews::create([
                    'uuid' => $item['uuid'],
                    'title' => $item['title'],
                    'description' => $item['description'],
                    'url' => $item['url'],
                    'image_url' => $item['image_url'],
                    'language' => $item['language'],
                    'published_at' => $item['published_at'],
                    'source' => $item['source'],
                    'categories' => json_encode($item['categories']),
                    'relevance_score' => $item['relevance_score'],
                ]);

                $apiNews->save();

                // Event dispatched
                NewsApiFetchEvent::dispatch($apiNews);

                $this->info('News fetched!');
                $bar->advance();
            }

            $bar->finish();

            return;
        }

        // Response boolean || failed
        $this->error('Unable to run the command');
        Log::error('NewsApi FAILED: '.print_r($response, true));
    }
}
