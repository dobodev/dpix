<?php

namespace App\Console;

use App\Console\Commands\TheNewsApiFetchCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // #1 Scheduler to run a command, depending on the Laravel scheduler || Server's Cron job
        $schedule->command(TheNewsApiFetchCommand::class)
        ->everyFifteenMinutes()
        ->evenInMaintenanceMode()
        ->withoutOverlapping();

        // #2 Scheduler Run a Closure
        $schedule->call(function () {
            Log::info('Scheduler is working OK!');
        })
        ->name('logger-working-ok')
        ->everyFiveMinutes()
        ->withoutOverlapping()
        ->onOneServer();

        // Scheduler run a system command
        // $schedule->exec('pwd >> /storage/logs/laravel.log 2>&1')->everyMinute();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
