<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class TheNewsApiService
{
    public function getAPIData(): array|bool
    {
        $key = config('services.thenewsapi.key');
        $url = config('services.thenewsapi.url');

        // $response = Http::get($url.'/all?api_token='.$key.'&language=en&categories=general,science,business,tech,politics');
        $response = Http::get($url.'?', [
            'api_token' => $key,
            'language' => 'en',
            'categories' => 'general,science,business,tech,politics',
        ]);

        if ($response->ok()) {
            return $response->json()['data'];
        }

        return $response->failed();
    }
}
