<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiNews extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'title',
        'description',
        'url',
        'image_url',
        'language',
        'published_at',
        'source',
        'categories',
        'relevance_score',
    ];

    protected $casts = [
        'published_at' => 'datetime',
    ];
}
