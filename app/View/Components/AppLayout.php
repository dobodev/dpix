<?php

namespace App\View\Components;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;
use Illuminate\View\View;

class AppLayout extends Component
{
    public function __construct(
        public ?Authenticatable $authUser,
    ) {
        $this->authUser = $this->getAuthUser();
    }

    /**
     * Get the view / contents that represents the component.
     */
    public function render(): View
    {
        return view('layouts.app');
    }

    public function getAuthUser(): ?Authenticatable
    {
        $guards = array_keys(config('auth.guards'));

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                $this->authUser = Auth::guard($guard)->user();
                break;
            }
        }

        return $this->authUser;
    }
}
