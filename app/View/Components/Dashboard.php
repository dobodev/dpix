<?php

namespace App\View\Components;

use App\Models\Meeting;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class Dashboard extends Component
{
    public $meetings;

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $this->meetings = Meeting::with('citizen', 'lawyer')->paginate(5); // eager loading with pagination

        return view('components.dashboard');
    }
}
