<?php

namespace App\View\Components\Admin;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Profile extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $user = 'Administrator',
        public string $password = '123456789'
    ) {
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.admin.profile');
    }

    public function getProfile(): array
    {
        return [
            'user' => $this->user,
            'password' => $this->password,
        ];
    }
}
