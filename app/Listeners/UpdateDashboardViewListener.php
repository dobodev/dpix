<?php

namespace App\Listeners;

use App\Events\NewsApiFetchEvent;
use Illuminate\Support\Facades\Log;

class UpdateDashboardViewListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(NewsApiFetchEvent $event)
    {
        Log::info('NewsApiFetchEvent triggered and, Listener is working: '.json_encode($event->data));
        // return redirect()->route('login');
    }
}
