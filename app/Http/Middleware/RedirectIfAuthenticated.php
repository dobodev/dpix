<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                // var_dump($guards);exit;
                return redirect(RouteServiceProvider::HOME);
            }

            // TODO implement different views based on user
            // if ($guard == "lawyer" && Auth::guard($guard)->check()) {
            //     return redirect('/lawyer');
            // }
            // if ($guard == "citizen" && Auth::guard($guard)->check()) {
            //     return redirect('/citizen');
            // }
            // if (Auth::guard($guard)->check()) {
            //     // return redirect('/home');
            //     return redirect(RouteServiceProvider::HOME);
            // }
            // Testing
        }

        return $next($request);
    }
}
