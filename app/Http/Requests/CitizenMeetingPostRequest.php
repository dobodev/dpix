<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CitizenMeetingPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        if (
            Auth::guard('citizen')->check() &&
            $this->request->get('user') == Auth::guard('citizen')->user()->id
        ) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'meeting_time' => ['required', 'date_format:Y-m-d H:i'],
            'description' => ['string', 'max:300'],
            'lawyer' => ['required', 'exists:App\Models\Lawyer,id'],
            'user' => ['required', 'exists:App\Models\Citizen,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'meeting_time.required' => 'Pickup date and meeting time.',
            'description.string' => 'Description must be a string up to 300 characters.',
            'lawyer.required' => 'Lawyer field is required.',
            'lawyer.exists' => 'A valid Lawyer ID is required.',
            'user.required' => 'Something wrong with your session. Please logout, and try again.',
            'user.exists' => 'Something wrong with your session. Please logout, and try again!.',
        ];
    }
}
