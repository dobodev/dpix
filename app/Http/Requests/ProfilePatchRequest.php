<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfilePatchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        // Multiple tables unique validation
        return [
            'name' => ['string', 'max:255'],
            'email' => ['email', 'max:255',
                'unique:lawyers,email,'.$this->user()->id,
                'unique:citizens,email,'.$this->user()->id,
                'unique:users,email,'.$this->user()->id,
            ],
            'available' => ['boolean'],
        ];
    }

    public function messages(): array
    {
        return [
            'name.string' => 'Name must be a string up to 255 characters.',
            'email.email' => 'Email must be a valid email address.',
            'email.unique' => 'Email already in use in our application',
        ];
    }
}
