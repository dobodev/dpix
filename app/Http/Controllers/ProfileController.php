<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfilePatchRequest;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
            'carbon' => new Carbon(Carbon::now()),
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfilePatchRequest $profilePatchRequest): RedirectResponse
    {
        $profilePatchRequest->user()->fill($profilePatchRequest->validated());

        if ($profilePatchRequest->user()->isDirty('email')) {
            $profilePatchRequest->user()->email_verified_at = null;
        }

        $profilePatchRequest->has('available') ? $profilePatchRequest->user()->available = true : $profilePatchRequest->user()->available = false;

        $profilePatchRequest->user()->save();

        $profilePatchRequest->session()->flash('status', 'Profile has been updated!');
        $profilePatchRequest->session()->flash('statusMessage', 'Enjoy your new profile!');

        return Redirect::route('profile.edit');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current-password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect(RouteServiceProvider::HOME);
    }
}
