<?php

namespace App\Http\Controllers;

use App\Http\Requests\LawyerMeetingPatchRequest;
use App\Models\Meeting;
use App\Notifications\MeetingApproved;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;

class LawyerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Lawyer profile meeting status update
     */
    public function meetingUpdate(LawyerMeetingPatchRequest $lawyerMeetingPatchRequest): RedirectResponse
    {
        $lawyerMeetingPatchRequestValidated = $lawyerMeetingPatchRequest->validated();

        /** @var \App\Models\Meeting $meeting */
        $meeting = Meeting::findOrFail($lawyerMeetingPatchRequestValidated['id']);

        if ($lawyerMeetingPatchRequest->user()->id != $meeting->lawyer_id) {
            $lawyerMeetingPatchRequest->session()->flash('status', 'Meeting confirmation failed!');
            $lawyerMeetingPatchRequest->session()->flash('statusMessage', 'Not authorized to perform this action!');

            return redirect(RouteServiceProvider::PROFILE);
        }

        $status = $lawyerMeetingPatchRequest['status'];

        /** @var \App\Models\Citizen */
        $citizen = $meeting->citizen;

        /** @var \App\Models\Lawyer */
        $lawyer = $meeting->lawyer;

        $meeting->update([
            'status' => $status,
        ]);

        // Notify the participant of the meeting
        $citizen->notify(new MeetingApproved($status));
        $lawyer->notify(new MeetingApproved($status));

        $lawyerMeetingPatchRequest->session()->flash('status', "Meeting {$status} successfully");
        $lawyerMeetingPatchRequest->session()->flash('statusMessage', 'You can check your scheduled meetings in the profile section!');

        return redirect(RouteServiceProvider::HOME);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {
        //
    }
}
