<?php

namespace App\Http\Controllers;

use App\Models\ApiNews;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     */
    public function index()
    {
        $apiNews = ApiNews::latest()->first();

        return view('home', [
            'apiNews' => $apiNews,
        ]);
    }
}
