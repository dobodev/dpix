<?php

namespace App\Http\Controllers;

use App\Http\Requests\CitizenMeetingPostRequest;
use App\Models\Lawyer;
use App\Models\Meeting;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;

class CitizenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Create meeting
     */
    public function createMeeting()
    {
        $lawyers = Lawyer::available()->get();

        return view('layouts.meetings', [
            'lawyers' => $lawyers,
        ]);
    }

   /**
    * Store meeting
    */
   public function storeMeeting(CitizenMeetingPostRequest $citizenMeetingPostRequest): RedirectResponse
   {
       $citizenMeetingPostRequestValidated = $citizenMeetingPostRequest->validated();

       Meeting::create([
           'citizen_id' => $citizenMeetingPostRequestValidated['user'],
           'lawyer_id' => $citizenMeetingPostRequestValidated['lawyer'],
           'description' => $citizenMeetingPostRequestValidated['description'],
           'meeting_time' => $citizenMeetingPostRequestValidated['meeting_time'],
       ]);

       $citizenMeetingPostRequest->session()->flash('status', 'Meeting created successfully');
       $citizenMeetingPostRequest->session()->flash('statusMessage', 'You can check your scheduled meetings in the profile section!');

       return redirect(RouteServiceProvider::HOME);
   }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {
        //
    }
}
