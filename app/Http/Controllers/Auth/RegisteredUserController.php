<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $occupation = ucfirst($request->get('occupation'));
        $model = "App\\Models\\{$occupation}";

        // Valid occupation for corresponding model creation
        if (! is_subclass_of($model, '\Illuminate\Database\Eloquent\Model')) {
            throw new \Exception('Not a valid occupation');
        }

        // TODO:: move to form request class
        if ($request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.$model],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'occupation' => ['required', 'in:lawyer,citizen,user'],
        ])) {
            $user = $model::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            event(new Registered($user));

            Auth::login($user);
        }

        return redirect(RouteServiceProvider::HOME);
    }
}
