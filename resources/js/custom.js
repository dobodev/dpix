// Profile edit, available toggle switch (lawyers)
 var toggleSwitch = document.getElementById('available');

 if (toggleSwitch) {
    toggleSwitch.addEventListener('change', function() {
        var isChecked = this.checked;

        var inputElement = document.getElementById('available');

        // Update the value of the input based on the toggle switch state
        inputElement.value = isChecked ? 1 : 0;
        inputElement.checked = isChecked;
    });
}

// Status Modal close
let modalStatus = document.getElementById('status');

if (modalStatus) {
    document.querySelector('.status-close').addEventListener('click', function() {
        document.getElementById('status').remove();
    });
}

// Flatpickr meeting calendar
flatpickr("#meeting_time", {
        enableTime: true,
        dateFormat: "Y-m-d H:i",
        altInput: true,
        altFormat: "F j, Y h:i K",
        // dateFormat: "Y-m-d",
        locale: {
            "firstDayOfWeek": 1 // start week on Monday
        },
        minDate: new Date(),
        maxDate: new Date().fp_incr(30), // 30 days from now
    });
