// Application bootstraping
import './bootstrap';

// Bootstrap framework
// import 'bootstrap/dist/js/bootstrap.min.js';
// import '@popperjs/core';

// Flatpickr package
import 'flatpickr';

// Custom js
import './custom';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
