<x-app-layout>
    <x-slot:heading class="italic">
        {{ __('Dashboard for meetings') }}
    </x-slot>

    <div class="py-12">
        {{-- Status Alert --}}
        @if ( session('status'))
            <x-status-modal :status="session('status')" :statusMessage="session('statusMessage')"/>
        @endif

        <div class="max-w-7xl mx-auto sm:px-4 lg:px-4 space-y-6">
            <div class="sm:p-6 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="w-full">
                    <h1 class="mb-6 text-4xl font-bold"> Latest News</h1>
                    <img src="{{ $apiNews->image_url }}" alt="Latest News Image"
                    class="w-full">
                    <ul class="list-disc hover:list-decimal">
                        <li class="p-2 text-teal-600 font-semibold"><span class="text-lg">Title:</span> {{ $apiNews->title }}</li>
                        <li class="p-2 text-teal-600 font-semibold"><span class="text-lg">Description:</span>{{ $apiNews->description }}</li>
                        <li class="p-2 text-teal-600 font-semibold"><span class="text-lg">Source URL:</span> <a href="{{ $apiNews->url }}"class="decoration-sky-500">{{ $apiNews->url }}</a></li>
                        <li class="p-2 text-teal-600 font-semibold"><span class="text-lg">Source:</span> {{ $apiNews->source }}</li>
                        <li class="p-2 text-teal-600 font-semibold"><span class="text-lg">Published At:</span> {{ $apiNews->published_at }}</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    {{-- Scheduled meetings dashboard table--}}
    <x-dashboard />

</x-app-layout>
