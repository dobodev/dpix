<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Request a meeting with lawyer') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        {{-- Styles --}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100 dark:bg-gray-900">
            @include('layouts.navigation')

            <!-- Page Heading -->
            @if (isset($header))
                <header class="bg-white dark:bg-gray-800 shadow">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif

            
            {{-- {{ $user = Auth::user()->get('id') }} --}}
            {{-- {{ $user = Auth::id() }} --}}
            {{-- {{ $lawyers = $lawyer->all() }} --}}
           

            <form method="POST" action="{{ route('profile.meeting-request') }}" aria-label="{{ __('Request') }}">
                @csrf
                <div class="form-group">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="lawyer">Lawyers</label>
                        </div>
                        <select class="custom-select" name="lawyer" id="lawyer">
                            <option >Select from the list</option>
                            @foreach ($lawyer->all() as $lawyer)
                                <option value="{{ $lawyer->id }}"> {{ $lawyer->name }}</option>                            
                            @endforeach
                        </select>
                      </div>
                </div>
                <div class="form-group">
                    <label for="description">Additional Info</label>
                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                  </div>                               
                
                <div class="form-group">                    
                    <label for="meeting_time"
                        class="col-md-4 col-form-label text-md-end">{{ __('Date & Time') }}</label>

                    <div class="col-md-6">
                        <input type="datetime-local" class="form-control" name="meeting_time">
                    </div>
                </div>

                <input id="invisible_id" name="citizen" type="hidden" value="{{  Auth::id() }}">
                <x-primary-button class="ml-3">
                    {{ __('Request') }}
                </x-primary-button>
              </form>

            <!-- Page Content -->
            {{-- <main>
                {{ $slot }}
            </main> --}}
        </div>        
        
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
        <script>
             config = {
                enableTime: true,
                dateFormat: 'Y-m-d H:i',
            }
            flatpickr("input[type=datetime-local]", config);
            // flatpickr("input[type=datetime-local]");
        </script>

        {{-- <script type="text/javascript"> --}}
        {{-- // $(document).ready(function(){
        //     // $('#demo').datetimepicker({
            
        //     // });
        //     jQuery('#datetimepicker').datetimepicker();
        // }); --}}
        {{-- // </script> --}}

    </body>
</html>





