@extends(backpack_view('blank'))

@php
    // if (backpack_theme_config('show_getting_started')) {
    //     $widgets['before_content'][] = [
    //         'type'        => 'view',
    //         'view'        => backpack_view('inc.getting_started'),
    //     ];
    // } else {
    //     $widgets['before_content'][] = [
    //         'type'        => 'jumbotron',
    //         'heading'     => trans('backpack::base.welcome'),
    //         'content'     => trans('backpack::base.use_sidebar'),
    //         'button_link' => backpack_url('logout'),
    //         'button_text' => trans('backpack::base.logout'),
    //     ];
    // }


    $widgets['before_content'][] = [
            'type'        => 'jumbotron',
            'heading'     => trans('backpack::base.welcome'),
            'content'     => trans('backpack::base.use_sidebar'),
            'button_link' => backpack_url('logout'),
            'button_text' => trans('backpack::base.logout'),
        ];


        $widgets['before_content'][] = [
            'type'         => 'alert',
            'class'        => 'alert alert-danger mb-2',
            'heading'      => 'Important information!',
            'content'      => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti nulla quas distinctio veritatis provident mollitia error fuga quis repellat, modi minima corporis similique, quaerat minus rerum dolorem asperiores, odit magnam.',
            'close_button' => true, // show close button or not
        ];
@endphp



@section('content')
    {{-- inc a component --}}
    {{-- <x-admin.profile /> --}}

    {{--inc a html  --}}
    {{-- <div class="col-md-6 col-lg-4">
        <div class="card">
        <div class="card-header">
            <h3 class="card-title">Social Media Traffic</h3>
        </div>
        <table class="table card-table table-vcenter">
            <thead>
            <tr>
                <th>Network</th>
                <th colspan="2">Visitors</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Instagram</td>
                <td>3,550</td>
                <td class="w-50">
                <div class="progress progress-xs">
                    <div class="progress-bar bg-primary" style="width: 71.0%"></div>
                </div>
                </td>
            </tr>
            <tr>
                <td>Twitter</td>
                <td>1,798</td>
                <td class="w-50">
                <div class="progress progress-xs">
                    <div class="progress-bar bg-primary" style="width: 35.96%"></div>
                </div>
                </td>
            </tr>
            <tr>
                <td>Facebook</td>
                <td>1,245</td>
                <td class="w-50">
                <div class="progress progress-xs">
                    <div class="progress-bar bg-primary" style="width: 24.9%"></div>
                </div>
                </td>
            </tr>
            <tr>
                <td>TikTok</td>
                <td>986</td>
                <td class="w-50">
                <div class="progress progress-xs">
                    <div class="progress-bar bg-primary" style="width: 19.72%"></div>
                </div>
                </td>
            </tr>
            <tr>
                <td>Pinterest</td>
                <td>854</td>
                <td class="w-50">
                <div class="progress progress-xs">
                    <div class="progress-bar bg-primary" style="width: 17.08%"></div>
                </div>
                </td>
            </tr>
            <tr>
                <td>VK</td>
                <td>650</td>
                <td class="w-50">
                <div class="progress progress-xs">
                    <div class="progress-bar bg-primary" style="width: 13.0%"></div>
                </div>
                </td>
            </tr>
            <tr>
                <td>Pinterest</td>
                <td>420</td>
                <td class="w-50">
                <div class="progress progress-xs">
                    <div class="progress-bar bg-primary" style="width: 8.4%"></div>
                </div>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
    </div> --}}
@endsection
