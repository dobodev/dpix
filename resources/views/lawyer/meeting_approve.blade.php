<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'View meetings') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        {{-- Styles --}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        {{-- <link href="{{ asset('css/meetings_table.css') }}" rel="stylesheet"> --}}
        @vite(['resources/css/meetings_table.css', 'resources/js/meeting_table.js'])


        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100 dark:bg-gray-900">
            @include('layouts.navigation')

            <!-- Page Heading -->
            @if (isset($header))
                <header class="bg-white dark:bg-gray-800 shadow">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif

            {{-- Table --}}
            <table class="container">
                <thead>
                    <tr>
                        <th> Citizen </th>
                        <th> Lawyer </th>
                        <th> Description </th>     
                        <td> Meeting Time </td>
                        <th> Edit </th>                        
                        <th> Approve </th>                        
                        <th> Delete </th>                        
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ App\Models\User::getUserNameByID(1) }} --}}
                    @foreach(App\Models\Meeting::all() as $meeting)
                    <tr>
                        <td> {{$meeting->citizen->name}} </td>
                        <td> {{$meeting->lawyer->name}} </td>
                        <td> {{$meeting->description}} </td>
                        <td> {{$meeting->meeting_time}} </td>
                        <td> <x-primary-button class="ml-3">{{ __('Edit') }}</x-primary-button> </td>
                        <td> <x-secondary-button class="ml-3">{{ __('Approve') }}</x-secondary-button> </td>
                        <td> <x-danger-button class="ml-3 danger">{{ __('Delete') }}</x-danger-button> </td>
                    </tr>
                    @endforeach
            </tbody>
            </table>
            {{-- Table --}}
                
           
              
        </div>        
        
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
        <script>
             config = {
                enableTime: true,
                dateFormat: 'Y-m-d H:i',
            }
            flatpickr("input[type=datetime-local]", config);
            // flatpickr("input[type=datetime-local]");
        </script>
    </body>
</html>







