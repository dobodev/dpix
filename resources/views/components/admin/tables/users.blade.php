<header>
    <h2 class="text-xl font-medium text-gray-900 dark:text-gray-100">
        {{ __('Manage Users') }}
    </h2>
</header>

<div class="user-meetings-table">
    <ul class="responsive-table">
      <li class="table-header">
        <div class="col col-1">ID</div>
        <div class="col col-2">Name</div>
        <div class="col col-3">Email</div>
        <div class="col col-4">Created At</div>
        <div class="col col-4">Edit</div>
        <div class="col col-4">Delete</div>
      </li>

      @foreach ($getUsersAll as $user)
        <li class="table-row list-item">
          <div class="col col-1" data-label="User Id">{{ $user->id }}</div>
          <div class="col col-2" data-label="User name">{{ $user->name }}</div>
          <div class="col col-3" data-label="User Email">{{ $user->email }}</div>
          <div class="col col-4" data-label="Created At"> {{ $user->created_at }}</div>
        </li>
      @endforeach
    </ul>
  </div>
