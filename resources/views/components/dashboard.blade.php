    <table class="dashboard">
        <thead>
            <tr>
                <th><h1>Meeting ID</h1></th>
                <th><h1>Laywer</h1></th>
                <th><h1>Citizen</h1></th>
                <th><h1>Case description</h1></th>
                <th><h1>Meeting Time</h1></th>
                <th><h1>Status</h1></th>
                <th><h1>Created</h1></th>
                <th><h1>Updated</h1></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($meetings as $meeting )
                <tr>
                    <td> {{ $meeting->id }} </td>
                    <td> {{ $meeting->lawyer->name }} </td>
                    <td> {{ '***' . Str::substr($meeting->citizen->name, -3) }} </td>
                    <td> {{ Str::substr($meeting->description, 0, 5) . '...' }} </td>
                    <td> {{ $meeting->meeting_time }} </td>
                    <td> {{ $meeting->status }} </td>
                    <td> {{ $meeting->created_at }} </td>
                    <td> {{ $meeting->updated_at }} </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $meetings->links() }}
