<header class="dark:bg-gray-800 shadow">
    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <h2 class=" {{ $heading->attributes['class'] }} font-bold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ $heading }}
        </h2>
    </div>
</header>



