<header>
    <h2 class="text-xl font-medium text-gray-900 dark:text-gray-100">
        {{ __('Scheduled Meetings') }}
    </h2>
</header>

<div class="user-meetings-table">
    <ul class="responsive-table">
    <li class="table-header">
        <div class="col col-1">Meeting Id</div>
        <div class="col col-2">Citizen Name</div>
        <div class="col col-3">Lawyer Name</div>
        <div class="col col-4">Meeting description</div>
        <div class="col col-5">Meeting Time</div>
        <div class="col col-4">Status </div>
    </li>
    @foreach ($user->meetings as $meeting)
        <li class="table-row list-item
            @if ($meeting->meeting_time->toDateString() < $carbon->toDateString())
                archived
            @elseif ($carbon->gte($meeting->meeting_time->subHours(3))))
                incoming
            @else
                bg-teal-500
            @endif
        ">
        <div class="col col-1" data-label="Meeting Id">{{ $meeting->id }}</div>
        <div class="col col-2" data-label="Meeting Id">{{ $meeting->citizen->name }}</div>
        <div class="col col-3" data-label="Lawyer Name">{{ $meeting->lawyer->name }}</div>
        <div class="col col-4" data-label="Description"> {{ $meeting->description }}</div>
        <div class="col col-5" data-label="Meeting Time">{{ $meeting->meeting_time }}</div>
        @if (Auth::guard('lawyer')->check())
            <div class="col col-4" data-label="Status">
            @if ($meeting->status == 'Pending')
                @if ($meeting->meeting_time->lt($carbon))
                    <span class="inline-flex items-center rounded-md bg-lime-300 px-2 py-1 text-lg font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">{{ $meeting->status }}</span>
                @else
                    <form action="{{ route('meeting.update') }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="id" value="{{ $meeting->id }}">
                        <button type="submit" name="status" value="accepted" class="bg-green-500 text-white font-bold py-2 px-4 rounded">Approve</button>
                        <button type="submit" name="status" value="rejected" class="bg-red-500 text-white font-bold py-2 px-4 rounded">Reject</button>
                    </form>
                @endif
            @else
                <span class="inline-flex items-center rounded-md bg-lime-300 px-2 py-1 text-lg font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">{{ $meeting->status }}</span>
            @endif
            </div>
        @else
            <div class="col col-4" data-label="Status">
                <span class="inline-flex items-center rounded-md bg-lime-300 px-2 py-1 text-lg font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">{{ $meeting->status }}</span>
            </div>
        @endif
        </li>
    @endforeach
    </ul>
</div>

