@props(['disabled' => false])

<label class="relative inline-flex cursor-pointer items-center">
    <input value="{{ $attributes['lawyer-available'] }}" {{ $attributes['lawyer-available'] == 1 ? 'checked' : '' }} type="checkbox" {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'peer sr-only']) !!} />
    <div
      class="peer flex h-8 items-center gap-4 rounded-full bg-orange-600 px-3 after:absolute after:left-1 after: after:h-6 after:w-9 after:rounded-full after:bg-white/40 after:transition-all after:content-[''] peer-checked:bg-blue-600 peer-checked:after:translate-x-full peer-focus:outline-none dark:border-slate-600 dark:bg-slate-700 text-sm text-white"
    >
      <span>No</span>
      <span>Yes</span>
    </div>
</label>
