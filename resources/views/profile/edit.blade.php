<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>

    <div class="py-12">
        {{-- Status Alert --}}
        @if ( session('status'))
            <x-status-modal :status="session('status')" :statusMessage="session('statusMessage')"/>
        @endif

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            {{-- User meetings table --}}
            {{-- Only meetings for logged in users of guards ['lawyer', 'citizen'] --}}
            @if (Auth::guard('lawyer')->check() || Auth::guard('citizen')->check())
                <div class="p-4 sm:p-8 bg-stone-300 dark:bg-gray-800 shadow sm:rounded-lg">
                    <div class="w-full">
                        <x-tables.user-meetings-table :user="$user" :carbon="$carbon"/>
                    </div>
                </div>
            @endif

            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('profile.partials.update-profile-information-form')
                </div>
            </div>

            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('profile.partials.update-password-form')
                </div>
            </div>

            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('profile.partials.delete-user-form')
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
