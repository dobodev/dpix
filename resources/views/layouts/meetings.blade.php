<x-app-layout>
    <x-slot:heading>
        {{ __('Create meeting') }}
    </x-slot>

    <div class="meeting-container mx-auto mt-8">
      <form action="{{ route('store-meeting') }}" method="POST" class="max-w-2xl mx-auto bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        @csrf
        <div class="mb-4">
            <label for="datetimepicker"
              class="block text-gray-700 text-sm font-bold mb-2"
            >Date and Time:</label>
            <input type="datetime-local"
              name="meeting_time"
              id="meeting_time"
              value="{{ old('meeting_time') }}"
              class="w-full  @error('meeting_time') is-invalid @enderror"
            >
            @error('meeting_time')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-4">
            <label for="textarea" class="block text-gray-700 text-sm font-bold mb-2">Meeting Agenda</label>
            <textarea id="textarea"
              name="description"
              rows="5"
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('description') is-invalid @enderror"
              placeholder="Short description ..."
            > {{ old('description') }} </textarea>
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-8">
        <label for="dropdown" class="block text-gray-700 text-sm font-bold mb-2">Lawyers:</label>
        <select id="dropdown"
            name="lawyer"
            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('lawyer') is-invalid @enderror"
        >
            <option value="" disabled selected>Select available Lawyer</option>
            @foreach ($lawyers as $lawyer)
                <option value="{{ $lawyer->id }}"
                    {{ old('lawyer') == $lawyer->id ? 'selected' : ''}}
                > {{ $lawyer->name }} </option>
            @endforeach
        </select>
        @error('lawyer')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <input type="hidden" name="user" value="{{ Auth::id() }}">
        @error('user')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="flex items-center justify-between">
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Request a meeting</button>
        </div>
      </form>
    </div>
</x-app-layout>
