<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>{{ $title ?? config('app.name') }}</title>
        <link rel="icon" type="image/x-icon" href="images/logo.svg">

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Vite load scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>

    <body class="font-sans antialiased" background="{{ asset('images/background.jpg') }}">
        <div class="min-h-screen dark:bg-gray-900">

            {{-- Navigation inc --}}
            <x-navigation :authUser="$authUser" />

            <!-- Page Heading -->
            @if (isset($heading))
                <x-heading :heading="$heading" />
            @endif

            <!-- Status Modal Alert -->
            <x-modal $name="status" :show="session('status')"/>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>

            {{-- <div class="container">
                {{-- @yield('content') --}}
                {{-- {{ $slot }} --}}
            {{-- </div> --}}
        </div>
        {{-- Footer --}}
        <x-footer />
    </body>
</html>
