<?php

namespace Tests\Unit;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function test_that_true_is_true(): void
    {
        $this->assertTrue(true);
    }

    public function test_that_false_is_false(): void
    {
        $this->assertFalse(false);
    }

    #[DataProvider('validNames')]
    public function testValidName(string $input, string $outcome, bool $expected): void
    {
        // dump($name, $outcome);
        $this->assertSame($input, $outcome);
    }

    public static function validNames(): array
    {
        return [
            'John' => ['John', 'John', true],
            'Jane' => ['Jane', 'Jane', true],
            'Jim' => ['Jim', 'Jimz', false],
        ];
    }
}
