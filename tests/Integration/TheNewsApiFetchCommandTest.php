<?php
declare(strict_types=1);

namespace Tests\Integration;

use App\Console\Commands\TheNewsApiFetchCommand;
use App\Services\TheNewsApiService;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class TheNewsApiFetchCommandTest extends TestCase
{
    private $theNewsApiService;

    protected function setUp(): void
    {
        $this->theNewsApiService = $this->createMock(TheNewsApiService::class);
    }

    protected function tearDown(): void
    {
        $this->theNewsApiService = null;
    }

    public function testTheNewsApiServiceFetchData(): void
    {
        $this->theNewsApiService->expects($this->once())
            ->method('getAPIData')
            ->willReturn([
                'news' => 'Some news',
                'content' => 'Some content',
            ]);

        // TODO: trigger the method via the artisan command?
        // $this->artisan('dobodev:the-news-api-fetch')->assertExitCode(0);
        $result = $this->theNewsApiService->getAPIData();

        $this->assertIsArray($result, 'Result is not valid array');
    }
}
