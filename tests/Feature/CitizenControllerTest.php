<?php

namespace Tests\Feature;

use App\Models\Citizen;
use App\Models\Lawyer;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CitizenControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function testCitizenGuardCanSeePage(): void
    {
        $citizen = Citizen::find(1);

        $response = $this
            ->actingAs($citizen, 'citizen')
            ->get('/citizen/create-meeting');

        $response->assertStatus(200);
    }

    public function testLawyerGuardCanNotSeePage(): void
    {
        $lawyer = Lawyer::find(1);

        $response = $this
        ->actingAs($lawyer, 'lawyer')
        ->get('/citizen/create-meeting');

        $response->assertStatus(302);
    }

    public function testWebGuardCanNotSeePage(): void
    {
        $user = User::find(1);

        $response = $this
        ->actingAs($user, 'web')
        ->get('/citizen/create-meeting');

        $response->assertStatus(302);
    }

    public function testCitizenCanCreateMeeting(): void
    {
        $citizen = Citizen::find(1);
        $lawyer = Lawyer::find(1);

        $response = $this
            ->actingAs($citizen, 'citizen')
            ->post('/citizen/store-meeting', [
                'user' => $citizen->id,
                'lawyer_id' => $lawyer->id,
                'description' => 'Test description',
                'meeting_time' => '2022-01-01 10:00:00'
            ]);

        $response->assertStatus(302);
    }



}
