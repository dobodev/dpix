<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\Models\ApiNews;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    private $apiNews;
    protected function setUp(): void
    {
        dd(ApiNews::latest()->first());
        // $this->apiNews = $this->createMock(ApiNews::class);
        $this->apiNews = ApiNews::latest()->first();
    }

    public function testHomePageNewsIsDisplayed(): void
    {
        // $this->apiNews->categories = 'test';
        // $this->apiNews->title = 'test';
        // $this->apiNews->description = 'test';
        // dd($this->apiNews);

        $response = $this->get('/');

        // $response->assertContent($this->apiNews);

        $response->assertStatus(200);
    }
}
