<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\Console\Commands\TheNewsApiFetchCommand;
use App\Services\TheNewsApiService;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class TheNewsApiFetchCommandTest extends TestCase
{
    public function testTheNewsApiFetchCommandExists(): void
    {
        $this->assertTrue(class_exists(TheNewsApiFetchCommand::class));
    }

    public function testTheNewsApiFetchCommandCanRun(): void
    {
        $this->artisan('dobodev:the-news-api-fetch')->assertExitCode(0);
    }

    public function testTheNewsApiFetchCommandCanRunWithArguments(): void
    {
        $this->artisan('dobodev:the-news-api-fetch', ['newsCounter' => 10])->assertExitCode(0);
    }

}
