<div align="center">
  <a href="https://gitlab.com/dobodev/dpix">
    <img src="public/images/logo.svg" alt="Logo" width="80" height="80">
  </a>
  <h3 align="center">Meeting Scheduler Citizens/Lawyers</h3>
  <p align="center"></p>
</div>


<!-- TABLE OF CONTENTS -->
<details open>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>


<!-- ABOUT THE PROJECT -->
## About The Project

![Screenshot](public/screenshots/Meeting_Scheduler_1.png)

![Screenshot](public/screenshots/Meeting_Scheduler_2.png)

![Screenshot](public/screenshots/Meeting_Scheduler_3.png)

![Screenshot](public/screenshots/Meeting_Scheduler_4.png)

![Screenshot](public/screenshots/Meeting_Scheduler_5.png)

![Screenshot](public/screenshots/Meeting_Scheduler_6.png)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


### Built With

* [![Laravel][Laravel.com]][Laravel-url]
* [![PHP][PHP.net]][PHP-url]
* [![MySQL][MySQL.com]][MySQL-url]
* [![Javascript][JavaScript.org]][JavaScript-url]
* [![Tailwind][Tailwindcss.com]][Tailwindcss-url]
* [![NPM][Npmjs.com]][Npmjs-url]
* [![Composer][Composer.com]][Composer-url]


<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

Web Application for managing meetings between lawyers and citizens. Meetings dashboard. Separate authentication for lawyers/citizens. CRUD over the separate models. Meetings approval and email notifications. Separate dashboards with active/archived/incoming meetings into profile section. Lawyer status {availability} active/inactive. REST API integration/consuming/exposing. Latest news exposing to the interface. Cron jobs and scheduler integrated. Custom commands implementation.

### Prerequisites
 <ul>
    <li><strong>PHP > 8.0</strong></li>
    <li><strong>MySQL > 8.0</strong></li>
    <li><strong>Apache | Nginx</strong></li>
    <li><strong>Composer</strong></li>
    <li><strong>NPM</strong></li>

  </ul>

### Installation
1. **Clone the repository**: into your local machine server directory /dpix
- git clone https://gitlab.com/dobodev/dpix/
2. **Navigate to the project directory**:
- cd dpix

### localhost || VM
- **Database Setup**:
-- Rename the .env.example => .env. Set the correct DB credentials
-- Run console command: php artisan migrate (migrate the database schema)

**Install Prerequisites**
-- Run console command: composer install (install the required packages)
-- Run console command: npm run build (install the application assets)

- **Serve the app**
-- Run your local server in the app root directory ( OR use laravel built in server instance -- php artisan serve)

### Dockerize (assuming the host machine has installed Docker Engine && || Docker Desktop)
- **docker compose up --build**
- *(default run setup): go to  http://localhost:8008/*

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
[Laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com/
[PHP.net]: https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white
[PHP-url]: https://www.php.net/
[MySQL.com]: https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white
[MySQL-url]: https://www.mysql.com/
[Tailwindcss.com]: https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=for-the-badge&logo=tailwind-css&logoColor=white
[Tailwindcss-url]: https://tailwindcss.com/
[JavaScript.org]: https://img.shields.io/badge/-JavaScript-F7DF1E?style=flat-square&logo=javascript&logoColor=black
[JavaScript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
[Npmjs.com]: https://img.shields.io/badge/npm-CB3837?style=for-the-badge&logo=npm&logoColor=white
[Npmjs-url]: https://www.npmjs.com/
[Composer.com]: https://img.shields.io/badge/Composer-885630?style=for-the-badge&logo=Composer&logoColor=white
[Composer-url]: https://getcomposer.org/




