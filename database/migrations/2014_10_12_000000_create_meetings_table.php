<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->id();
            $table->text('description');
            $table->unsignedBigInteger('citizen_id');
            $table->unsignedBigInteger('lawyer_id');
            $table->timestamp('meeting_time');
            $table->timestamps();
            $table->foreign('citizen_id')->references('id')->on('citizens');
            $table->foreign('lawyer_id')->references('id')->on('lawyers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('meetings');
    }
};
