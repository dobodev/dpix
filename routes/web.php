<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CitizenController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LawyerController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
require __DIR__.'/auth.php';

/*
|--------------------------------------------------------------------------
| Dashboard
|--------------------------------------------------------------------------
*/
Route::get('/', [HomeController::class, 'index'])->name('dashboard');

/*
|--------------------------------------------------------------------------
| Citizen
|--------------------------------------------------------------------------
*/
Route::group(
    [
        'prefix' => 'citizen',
        'middleware' => ['auth:citizen'],
    ],
    function () {
        Route::get('/create-meeting', [CitizenController::class, 'createMeeting'])->name('create-meeting');
        Route::post('/store-meeting', [CitizenController::class, 'storeMeeting'])->name('store-meeting');
    }
);

/*
|--------------------------------------------------------------------------
| Lawyer
|--------------------------------------------------------------------------
*/
Route::group(
    [
        'prefix' => 'lawyer',
        'middleware' => ['auth:lawyer'],
    ],
    function () {
        Route::patch('/meeting/update/', [LawyerController::class, 'meetingUpdate'])->name('meeting.update');
    }
);

/*
|--------------------------------------------------------------------------
| Profile
|--------------------------------------------------------------------------
*/
Route::group(
    [
        'middleware' => ['auth:citizen,lawyer,web'],
    ],
    function () {
        Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    }
);

/*
|--------------------------------------------------------------------------
| Quick routes for Auth user logout|
|--------------------------------------------------------------------------
*/
Route::get('/logout', function () {
    Auth::logout();
    Route::get('/');
});

/*
|--------------------------------------------------------------------------
| Custom Admin Panel
|--------------------------------------------------------------------------
*/
// Route::resource('/admin', AdminController::class);

/*
|--------------------------------------------------------------------------
| Clear Cache
|--------------------------------------------------------------------------
*/
Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    echo 'Cleared!';
    Route::get('/');
});
