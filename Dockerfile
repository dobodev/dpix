# Use the official PHP image as the base image
FROM php:8.2-apache

# Install system dependencies && Install PHP extensions
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libzip-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd pdo pdo_mysql zip mbstring exif pcntl bcmath gd

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Enable Apache Rewrite Module
RUN a2enmod rewrite

# Install Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Set working directory
WORKDIR /var/www/html

# Copy existing application directory contents
COPY . .

# Copy custom Apache configuration file
COPY ./_apache/apache_laravel.conf /etc/apache2/sites-available/000-default.conf

# Generate application key if not set
# RUN if [ -z "$APP_KEY" ]; then \
#       composer install && \
#       php artisan key:generate --force && \
#       php artisan config:cache && \
#       php artisan route:cache && \
#       php artisan view:cache; \
#     fi

# Install Laravel dependencies
RUN composer install

# Ensure the storage and bootstrap cache directories have the correct permissions
RUN chown -R www-data:www-data /var/www/html /var/www/html/storage /var/www/html/bootstrap/cache
RUN chmod -R 755 /var/www/html/storage /var/www/html/bootstrap/cache

# Clear config cache
RUN php artisan config:cache

# Run database migrations
# RUN php artisan migrate --force

# Expose port 80
EXPOSE 80

# Start Apache
CMD ["apache2-foreground"]
